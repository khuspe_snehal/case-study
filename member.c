#include<stdio.h>
#include<stdlib.h>
#include<string.h>

#include"library.h"


void member_area(user_t *u) {
	int choice;
	char name[80];
	do {
		printf("\n\n0. Sign Out\n1. Find Book\n2. Edit Profile\n3. Change Password\n4. Book Availability\n4.issued books\nEnter choice: ");
		scanf("%d", &choice);
		switch(choice) {
			case 1:  //find book
					printf("enter book name");
					scanf("%s", name);
					book_find_by_name(name);
			 		break;
			case 2://edit profile
					edit_profile(u);
					break;
			case 3://change password
					change_password(u);
					break;
			case 4:  //book availability
					bookcopy_checkavail();
					break;
			case 5: //list issued books
					display_issued_bookcopies(u->id);
					break;		
		}
	}while (choice != 0);	
}





//ITS  CHECKING BOOK COPY AVAILABILITY

void bookcopy_checkavail()
{
	int book_id;
	FILE *fp;
	bookcopy_t bc;
	int count = 0;
	//input book id
	printf("enter the book id: ");
	scanf("%d",&book_id);
	//open book copies file
	fp = fopen(BOOKCOPY_DB, "rb");
	if(fp == NULL)
	{
		perror("cannot open bookcopies file");
		return;
	}
while (fread(&bc, sizeof(bookcopy_t), 1, fp) > 0) {
//if bookid is matching and status is available,count copies

	if(bc.bookid == book_id && strcmp(bc.status, STATUS_AVAIL) == 0){
		count++;
	}
	
}
//close the book copies file
fclose(fp);
printf("number of copies available: %d\n", count); 
 //print the message 



}


