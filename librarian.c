#include<stdio.h>
#include<stdlib.h>
#include<string.h>

#include"library.h"

void librarian_area(user_t *u) {
	int choice,memberid;
	char name[80];
	do {
		printf("\n\n0. Sign Out\n1. Add member\n2. Edit Profile\n3. Change Password\n4. Add Book\n5. Find Book\n6. Edit Book\n7. Check Availability\n8. Add Copy\n9. Change Rack\n10. Issue Copy\n11. Return Copy\n12. Take Payment\n13. Payment History\n14.display members\nEnter choice: ");
		scanf("%d", &choice);
		switch(choice) {
			case 1: //add member
					add_member();
				break;
			case 2:   //edit profile
					edit_profile(u);
					break;
			case 3://change password
				change_password(u);
				break;
			case 4:  //Add book
					add_book();
					break;
			case 5:  //find book
					printf("enter book name");
					scanf("%s",name);
					book_find_by_name(name);
					break;
			case 6: //edit book
					book_edit_by_id();
					break;
			case 7: //check availability
					bookcopy_checkavail_details();
					break;
			case 8: //Add copy
					bookcopy_add();
					break;
			case 9://change rack
					change_rack();
					break;
			case 10://issue book copy
					bookcopy_issue();
					break;
			case 11: //return copies
					bookcopy_return();                         //copy return 3rd
					break;
			case 12://take payment
					fees_payment_add();
					break;
			case 13: //payment history
					printf("enter member id of thr member: ");
					scanf("%d",&memberid);
					payment_history(memberid);
					break;
			case 14://display members

					display_members();	
					break;	
		}
	}while (choice != 0);		
}

void add_member()
{
	//input member details
	user_t u;
	user_accept(&u);

	//add librarian into user file
	user_add(&u);
}

void add_book() {
	FILE *fp;
	//input book details
	book_t b;
	book_accept(&b);
	b.id == get_next_book_id();

	//add book into the book file
    //open books file
	fp = fopen(BOOK_DB, "ab");
	if(fp == NULL){
			perror("cannot open books file");
			exit(1);

	}

	//opened book file
   fwrite(&b, sizeof(book_t), 1, fp);
   printf("book added in file.\n");

	//close books file
	fclose(fp);
}




void book_edit_by_id()
{
	int id, found = 0;
     FILE *fp;
	 book_t b;

	//input book id from user
	printf("enter book id: ");
	scanf("%d",&id);
	//open books file
	fp = fopen(BOOK_DB, "rb+");
	if(fp == NULL) {
		perror("cannot open book file");
		exit(1);
	}
	//read book one by one and see if book is matched with id 
	while (fread(&b, sizeof(book_t), 1, fp) > 0)
	{
		if(id == b.id){
			found = 1;
			break; 

		}
	}
	 
	 //if found

	 if(found) {
		 //input new book details from user
		long size = sizeof(book_t);
		book_t nb;
		book_accept(&nb);
		nb.id = b.id;
		//take file position one record behind
		fseek(fp, -size, SEEK_CUR);
		//OVERWRITE BOOK DETAILS INTO FILE
		fwrite(&nb, size, 1, fp);
		printf("book updated\n");
		
		}

		else
		//if not 
		//shoe msg to user that book is not found
		printf("book not found\n");
		//close books file
		fclose(fp);


	}

void bookcopy_add()
{   
	FILE *fp;
	//INPUT BOOK COPY DETAILS
	bookcopy_t b;
	bookcopy_accept(&b);
	b.id = get_next_bookcopy_id();

	//ADD BOOKCOPY INTO THE BOOKS FILE

	//OPEN BOOKCOPIES FILE
	fp = fopen(BOOKCOPY_DB, "ab");
	if (fp == NULL)
	{
		perror("cannot open book copies file");
		exit(1);
	}
	
	//APPEND BOOKcopy TO THE FILE
	fwrite(&b, sizeof(bookcopy_t), 1, fp);
	printf("bookcopy added in the file.\n");
	//CLOSE BOOKcopies FILE
	fclose(fp);

}

void bookcopy_checkavail_details()
{
	int book_id;
	FILE *fp;
	bookcopy_t bc;
	int count = 0;
	//input book id
	printf("enter the book id: ");
	scanf("%d", &book_id);
	//open book copies file
	fp = fopen(BOOKCOPY_DB, "rb");
	if(fp == NULL)
	{
		perror("cannot open bookcopies.");
		return;
	}

	//read bookcopy records one by one
	
	while (fread(&bc, sizeof(bookcopy_t), 1, fp) > 0) {
			//if book id is matching and status is avialable ,print copy details
			if(bc.bookid == book_id && strcmp(bc.status, STATUS_AVAIL) == 0)
			{
			bookcopy_display(&bc);
			count++;
			}
		}
		//close book copies file
		fclose(fp);
		//if no copy is available ,print the message
		if(count == 0)
			printf("no copies available.\n");

	

}



//2nd last BOOK COPY ISSUE


void bookcopy_issue()
{
		issuerecord_t rec;
		FILE *fp;
		//accept issuerecord details from user
		
			issuerecord_accept(&rec);
		//TODO: IF USER IS NOT PAID ,GIVE ERROR AND RETURN.
		//GENERATE AND ASSIGN NEW ID FOR THE ISSUERECORD
        //IF USER IS NOT PAID GIVE ERROR ,RETURN
		if (!is_paid_member(rec.memberid))
		{
			printf("member is not paid");
			return;
		}
		
		rec.id = get_next_issuerecord_id();
		//open issurecord file
		fp = fopen(ISSUERECORD_DB, "ab");             
		if(fp == NULL){
			perror("issuerecord file can not be opened");
			exit(1);
		}
		//append record into the file
		fwrite(&rec, sizeof(issuerecord_t), 1, fp);
		//close the file
		fclose(fp);
		//mark copy as issued 
        bookcopy_changestatus(rec.copyid, STATUS_ISSUED);
}


void bookcopy_changestatus(int bookcopy_id, char status[])
{
			bookcopy_t bc;
			FILE *fp;
			long size = sizeof(bookcopy_t);
			//open book copies file
			fp = fopen(BOOKCOPY_DB, "rb+");
			if(fp == NULL)
			{
				perror("cannot open book copies file");
				return;

			}
			//read book copies one by one
			while(fread(&bc, sizeof(bookcopy_t), 1, fp) > 0) 
			{
				//if book copy id is matching 
				if(bookcopy_id == bc.id)
				{

					//modify its status
					strcpy(bc.status, status);
					//go one record back

					fseek(fp, -size, SEEK_CUR);
					//OVERWRITE THE RECORD INTO THE FILE
					fwrite("&bc",sizeof(bookcopy_t), 1, fp);
					break;

				}

			}
			//close the file
			fclose(fp);

}




//3 rd copy return


void display_issued_bookcopies(int member_id)
{      
	FILE *fp;
	issuerecord_t rec;

	//open isuue records file
	fp = fopen(ISSUERECORD_DB, "rb");
	if(fp == NULL){
		perror("cannot open file");
		return;
	}
	//read records one by one
	while (fread(&rec, sizeof(issuerecord_t), 1, fp) > 0)
	//if member id is not matching and return date is zero,print it
	{
		if(rec.memberid == member_id && rec.return_date.day == 0)
				issuerecord_display(&rec);
		
	}
	//close the file
	fclose(fp);
	

}


void bookcopy_return()
{
	int member_id, record_id;
	FILE *fp;
	issuerecord_t rec;
	int diff_days, found = 0;
	long size = sizeof(issuerecord_t);
	//input member id
	printf("enter member id: ");
	scanf("%d", &member_id);
	//print all issued book(not returned yet)
	display_issued_bookcopies(member_id);
	//input issue record id to be returned 
	printf("enter issue record id(to return): ");
	scanf("%d", &record_id);
	//open issuerecord file
	fp = fopen(ISSUERECORD_DB, "rb");
	if(fp == NULL){
		perror("cannot open issue record file");
		return;

	}
	//read record one by one
	while(fread(&rec, sizeof(issuerecord_t), 1, fp) > 0) {
		// find issuerecord id
		if(record_id == rec.id) {
			found = 1;
			// initialize return date
			rec.return_date = date_current();
			// check for the fine amount
			diff_days = date_cmp(rec.return_date, rec.return_duedate);
			// update fine amount if any
			if(diff_days > 0) {
				rec.fine_amount = diff_days * FINE_PER_DAY;
				fine_payment_add(rec.memberid, rec.fine_amount);
				printf("fine amount Rs. %.2lf/- is applied.\n", rec.fine_amount);

			}
			break;
		}
	}
	
	if(found) {
		// go one record back
		fseek(fp, -size, SEEK_CUR);
		// overwrite the issue record
		fwrite(&rec, sizeof(issuerecord_t), 1, fp);
		// print updated issue record.
		printf("issue record updated after returning book:\n");
		issuerecord_display(&rec);
		// update copy status to available 
		bookcopy_changestatus(rec.copyid, STATUS_AVAIL);
	}
	
	// close the file.
	fclose(fp);

}



void change_rack()
{
	int copy_id,found=0;
	FILE *fp;
	bookcopy_t rec;
	printf("enter the copy id whose rack has to be changed: ");
	scanf("%d",&copy_id);

	fp = fopen(BOOKCOPY_DB, "rb+");
	if(fp == NULL)
	{
		perror("cannot open file");
		return;

	}
	while (fread(&rec, sizeof(bookcopy_t),1 ,fp)>0)
	{
		if(rec.id == copy_id)
		{
			found =1;
			break;
		}
	}
	if(found)
	{
		int rack;
		printf("enter the rack number to which the copy is to be transferred: ");
		scanf("%d",&rack);
		rec.rack=rack;
		fseek(fp, -sizeof(bookcopy_t), SEEK_CUR);
		fwrite(&rec, sizeof(bookcopy_t),1,fp);
		printf("rack changed successfully");
		bookcopy_display(&rec);


	}
	else
		printf("bookcopy not found\n");
		fclose(fp);

		

}



void fees_payment_add()
{
	FILE *fp;
	//accept fees payment
	payment_t pay;
	payment_accept(&pay);
	pay.id = get_next_payment_id();
	//open the file
	fp = fopen(PAYMENT_DB, "ab");
	if(fp==NULL)
	{
		perror("cannot open payment file");
		exit(1);

	}
	//append payment data at the end of the file
	fwrite(&pay, sizeof(payment_t),1,fp);
	//close the file
	fclose(fp);
}

void payment_history(int memberid)
{
	FILE *fp;
	payment_t pay;
	//open the file
	fp = fopen(PAYMENT_DB, "rb");
	if(fp == NULL)
	{
		perror("cannot open payment file");
		return;

	}
	//read payment one by one
	while (fread(&pay, sizeof(payment_t), 1, fp) > 0)
	{
		if(pay.memberid == memberid)
			payment_display(&pay);
		
	}
	//close file
	fclose(fp);

}

//CHECK PAID MEMBER

int is_paid_member(int memberid)
{
	date_t now = date_current();
	FILE  *fp;
	payment_t pay;
	int paid = 0;
	//open file
	fp = fopen(PAYMENT_DB, "rb");
	if(fp == NULL){
		perror("cannot open payment file");
		return 0;
	}
	//read payment one by one till EOF
	while(fread(&pay,sizeof(payment_t),1,fp)>0)
	{
		if(pay.memberid == memberid && pay.next_pay_duedate.day != 0 && date_cmp(now,pay.next_pay_duedate) < 0)
		{
			paid = 1;
			break;
		}
	}
	//close
	fclose(fp);
	return paid;
}



void fine_payment_add(int memberid, double fine_amount)
{
	FILE *fp;
	//initialize fine payment
	payment_t pay;
	pay.id = get_next_payment_id();
	pay.memberid = memberid;
	pay.amount = fine_amount;
	strcpy(pay.type, PAY_TYPE_FINE);
	pay.tx_time = date_current();
	memset(&pay.next_pay_duedate, 0, sizeof(date_t));
	//open file
	fp = fopen(PAYMENT_DB,"ab");
	if(fp == NULL)
	{
		perror("cannot find file payment");
		exit(1);
	}
	//append payment data at the end of the file
	fwrite(&pay,sizeof(payment_t),1, fp);
	//close file
	fclose(fp);
}


void display_members()
{
	user_t u;
	FILE *fp;
	fp = fopen(USER_DB, "rb");
	if(fp==NULL)
	{
		perror("cannot open issue record file");
		return;
	}
	while(fread(&u,sizeof(user_t),1,fp)>0)
	{
		if(strcmp(u.role,ROLL_MEMBER)==0)
		{
			user_display(&u);
		}
	}
	
	fclose(fp);		
	
	
}